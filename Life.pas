unit Life;

{$MODE Delphi}

interface

uses
  LCLIntf, LCLType, SysUtils, Classes,
  Graphics, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type

  { TForm1 }

  TForm1 = class(TForm)
    Image1: TImage;
    LT: TEdit;
    Timer1: TTimer;
    Hor: TScrollBar;
    Vert: TScrollBar;
    OD: TOpenDialog;
    GroupBox1: TGroupBox;
    SW: TButton;
    Button1: TButton;
    Button2: TButton;
    Button3: TButton;
    Button4: TButton;
    NN: TEdit;
    Res: TButton;
    SPD: TEdit;
    Button5: TButton;
    CheckBox1: TCheckBox;
    procedure FormCreate(Sender: TObject);
    procedure Button1Click2(Sender: TObject);
    procedure LTChange(Sender: TObject);
    procedure LTClick(Sender: TObject);
    procedure SWClick(Sender: TObject);
    procedure Image1Click(Sender: TObject);
    procedure ResClick(Sender: TObject);
    procedure Image1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
    procedure VertScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: integer);
    procedure HorScroll(Sender: TObject; ScrollCode: TScrollCode;
      var ScrollPos: integer);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure Button3Click(Sender: TObject);
    procedure Button4Click(Sender: TObject);
    procedure FormResize(Sender: TObject);
    procedure Button5Click(Sender: TObject);
  private
    { Private declarations }
  public
    { Public declarations }
  end;

const
  MAXRTR = 256;

type
  TrtrStates = (rcvQ, sndNC, sndQ, chkQ1, chkQ2, skipQ, rcvA, sndA, chkA1, chkA2, skipA);

  TrtrPacket = record
    rtrState: TrtrStates;
    rtrNextState: TrtrStates;
    rtrAddr: integer;
    rtrTime: byte;
    //rtrWait: byte;
  end;

var
  Form1: TForm1;
  //P: array [-1..1000, -1..1000] of boolean;
  rtrP: array [1..MAXRTR, 1..MAXRTR] of TrtrPacket;
  CX, CY, N, Lifetime, Zoom, IW, IH: integer;

implementation

function StateToColor(X, Y: integer): TColor;
var
  cellColor: integer;
begin

  if ((X < 1) or (Y < 1) or (X > N) or (Y > N)) then
    Exit;
  cellColor := ((Ord(rtrP[X, Y].rtrState) + 2) * $FFFFFF) div 14;
  Result := TColor(cellColor);
  //(rcv, snd, chk1, chk2, wait, sndNC);
end;

procedure Render;
var
  X, Y, BX, BY, Nt, Nx, Ny, iX, iY: integer;
begin
  Form1.Image1.Canvas.Brush.Color := clGray;
  Form1.Image1.Canvas.FillRect(Form1.Image1.ClientRect);

  //BX := CX - ((IW div 2) div Zoom);
  //BY := CY - ((IH div 2) div Zoom);

  //Form1.Image1.Canvas.Pen.Color := clRed;
  //Form1.Image1.Canvas.Rectangle((-BX) * Zoom, (-BY) * Zoom, (-BX + N) *
  //  Zoom, (-BY + N) * Zoom);
  //Form1.Image1.Canvas.Brush.Color := clWhite;
  //Form1.Image1.Canvas.Pen.Color := clBlack;
  Nt := N div Zoom;

  //CX := (N + (((Zoom - 1) * N * (2 * Form1.Hor.Position - Form1.Hor.Max)) div
  //  (Form1.Hor.Max * 8))) div 2;
  //CY := (N + (((Zoom - 1) * N * (2 * Form1.Vert.Position - Form1.Vert.Max)) div
  //  (Form1.Vert.Max * 8))) div 2;

  //CX := CX - (Nt div 2);
  //CY := CY - (Nt div 2);
  BX := IW div Nt;  // step X  in pixels
  BY := IH div Nt;  // step Y in pixels
  for iX := 1 to Nt do
    for iY := 1 to Nt do
      //      if P[X + BX, Y + BY] then
    begin
      Form1.Image1.Canvas.Brush.Color :=
        StateToColor(CX + iX - 1, CY + iY - 1);
      //clBlack;
      Form1.Image1.Canvas.Pen.Color :=
        StateToColor(CX + iX - 1, CY + iY - 1);
      //StateToColor(X, Y);//
      if ((BX > 2) and (BY > 2)) then
        Form1.Image1.Canvas.Rectangle((iX - 1) * BX, (iY - 1) * BY, iX * BX, iY * BY)
      else
        Form1.Image1.Canvas.Pixels[iX * BX + 1, iY * BY + 1] :=
          StateToColor(iX, iY);//clBlack;
    end;
  Form1.Image1.Canvas.Pen.Color := clSilver;
  if ((BX > 4) and (BY > 4)) then
  begin
    for X := 1 to Nt do
    begin
      Form1.Image1.Canvas.MoveTo(X * BX, 0);
      Form1.Image1.Canvas.LineTo(X * BX, IH);
      //X := X + BX;
    end;
    for Y := 1 to Nt do
    begin
      Form1.Image1.Canvas.MoveTo(0, Y * BY);
      Form1.Image1.Canvas.LineTo(IW, Y * BY);
      //Y := Y + BY;
    end;
  end;
end;
//(rcvQ,sndNC, sndQ, chkQ1, chkQ2, skipQ,rcvA,sndA,chkA1, chkA2, skipA );
procedure waitQ(Xs, Ys, Xd, Yd: integer);
begin
  if ((Xd = Xs) and (Yd = Ys)) then
    Exit;
  if ((Xd < 1) or (Yd < 1) or (Xd > N) or (Yd > N)) then
    Exit;
  if ((rtrP[Xs, Ys].rtrNextState = sndQ) or (rtrP[Xs, Ys].rtrNextState = sndA) or
    (rtrP[Xs, Ys].rtrNextState = sndNC)) then
    Exit;

  if ((rtrP[Xd, Yd].rtrState <> sndQ) and (rtrP[Xd, Yd].rtrState <> sndA) and
    (rtrP[Xd, Yd].rtrState <> sndNC)) then
    Exit;

  case rtrP[Xs, Ys].rtrState of
    rcvQ:
    begin
      if (rtrP[Xd, Yd].rtrState = sndNC) then
      begin
        rtrP[Xs, Ys].rtrNextState := rcvQ;
        Exit;
      end;
      // Retransmit New Q
      if ((rtrP[Xs, Ys].rtrTime <> rtrP[Xd, Yd].rtrTime) and
        (rtrP[Xs, Ys].rtrAddr <> rtrP[Xd, Yd].rtrAddr) and
        (rtrP[Xd, Yd].rtrState = sndQ)) then
      begin
        rtrP[Xs, Ys].rtrAddr := rtrP[Xd, Yd].rtrAddr;
        rtrP[Xs, Ys].rtrTime := rtrP[Xd, Yd].rtrTime;
        rtrP[Xs, Ys].rtrNextState := sndQ;
        Exit;
      end;
      // Retransmit Old A or Q
      if ((rtrP[Xs, Ys].rtrTime = rtrP[Xd, Yd].rtrTime) and
        (rtrP[Xs, Ys].rtrAddr = rtrP[Xd, Yd].rtrAddr)) then
      begin
        rtrP[Xs, Ys].rtrNextState := sndNC;
        Exit;
      end;
    end;
    sndNC:
    begin
      rtrP[Xs, Ys].rtrNextState := rcvQ;
      Exit;
    end;
    sndQ:
    begin
      rtrP[Xs, Ys].rtrNextState := chkQ1;
      Exit;
    end;
    // Check retransmission
    chkQ1:
    begin
      if (rtrP[Xd, Yd].rtrState = sndNC) then
      begin
        rtrP[Xs, Ys].rtrNextState := rcvQ;
        Exit;
      end;
      if ((rtrP[Xs, Ys].rtrTime = rtrP[Xd, Yd].rtrTime) and
        (rtrP[Xs, Ys].rtrAddr = rtrP[Xd, Yd].rtrAddr) and
        (rtrP[Xd, Yd].rtrState = sndQ)) then
      begin
        rtrP[Xs, Ys].rtrNextState := skipQ;
        Exit;
      end;
    end;
    chkQ2:
    begin
      if (rtrP[Xd, Yd].rtrState = sndNC) then
      begin
        rtrP[Xs, Ys].rtrNextState := rcvQ;
        Exit;
      end;
      if ((rtrP[Xs, Ys].rtrTime = rtrP[Xd, Yd].rtrTime) and
        (rtrP[Xs, Ys].rtrAddr = rtrP[Xd, Yd].rtrAddr) and
        (rtrP[Xd, Yd].rtrState = sndQ)) then
      begin
        if (rtrP[Xs, Ys].rtrAddr = (Xs + Ys * 256)) then
        begin
          rtrP[Xs, Ys].rtrNextState := sndA;
          Exit;
        end;
      end;
    end;
    skipQ:
    begin

    end;

    rcvA:
    begin
      if (rtrP[Xd, Yd].rtrState = sndNC) then
      begin
        rtrP[Xs, Ys].rtrNextState := rcvQ;
        Exit;
      end;
      if ((rtrP[Xs, Ys].rtrTime = rtrP[Xd, Yd].rtrTime) and
        (rtrP[Xs, Ys].rtrAddr = rtrP[Xd, Yd].rtrAddr) and
        (rtrP[Xd, Yd].rtrState = sndA)) then
      begin
        rtrP[Xs, Ys].rtrNextState := sndA;
        Exit;
      end;
    end;
    sndA:
    begin
      rtrP[Xs, Ys].rtrNextState := chkA1;
      Exit;
    end;
    chkA1:
    begin
      if (rtrP[Xd, Yd].rtrState = sndNC) then
      begin
        rtrP[Xs, Ys].rtrNextState := rcvQ;
        Exit;
      end;
      if ((rtrP[Xs, Ys].rtrTime = rtrP[Xd, Yd].rtrTime) and
        (rtrP[Xs, Ys].rtrAddr = rtrP[Xd, Yd].rtrAddr) and
        (rtrP[Xd, Yd].rtrState = sndA)) then
      begin
        rtrP[Xs, Ys].rtrNextState := skipA;
        Exit;
      end;
    end;
    chkA2:
    begin
      if (rtrP[Xd, Yd].rtrState = sndNC) then
      begin
        rtrP[Xs, Ys].rtrNextState := rcvQ;
        Exit;
      end;
      if ((rtrP[Xs, Ys].rtrTime = rtrP[Xd, Yd].rtrTime) and
        (rtrP[Xs, Ys].rtrAddr = rtrP[Xd, Yd].rtrAddr) and
        (rtrP[Xd, Yd].rtrState = sndA)) then
      begin
        rtrP[Xs, Ys].rtrNextState := rcvQ;
        Exit;
      end;
    end;
    skipA:
    begin
      rtrP[Xs, Ys].rtrNextState := rcvQ;
      Exit;
    end;
  end;
end;


procedure Turn;
var
  X, Y, i, j, r: integer;
begin

  for X := 1 to N do
    for Y := 1 to N do
    begin
      //(rcvQ,sndNC, sndQ, chkQ1, chkQ2, waitQ,rcvA,sndA,chkA1, chkA2, waitA );
      r := 1 + (Random(13) div 10);
      case rtrP[X, Y].rtrState of
        rcvQ:
        begin
          for i := -r to r do
            for j := -r to r do
              waitQ(X, Y, X + i, Y + j);
        end;
        sndNC:
        begin
          rtrP[X, Y].rtrNextState := rcvQ;
        end;
        sndQ:
        begin
          rtrP[X, Y].rtrNextState := chkQ1;
        end;
        chkQ1:
        begin
          for i := -r to r do
            for j := -r to r do
              waitQ(X, Y, X + i, Y + j);
          if (rtrP[X, Y].rtrNextState = chkQ1) then
            rtrP[X, Y].rtrNextState := chkQ2;
          //else
          //  rtrP[X, Y].rtrNextState := waitQ;
        end;
        chkQ2:
        begin
          for i := -r to r do
            for j := -r to r do
              waitQ(X, Y, X + i, Y + j);
          if (rtrP[X, Y].rtrNextState = chkQ2) then
            rtrP[X, Y].rtrNextState := sndQ;
        end;
        skipQ:
        begin
          if (rtrP[X, Y].rtrAddr = (X + Y * 256)) then
            rtrP[X, Y].rtrNextState := sndA
          else
            rtrP[X, Y].rtrNextState := rcvA;
        end;
        rcvA:
        begin
          for i := -r to r do
            for j := -r to r do
              waitQ(X, Y, X + i, Y + j);
        end;
        sndA:
        begin
          rtrP[X, Y].rtrNextState := chkA1;
        end;
        chkA1:
        begin
          for i := -r to r do
            for j := -r to r do
              waitQ(X, Y, X + i, Y + j);
          if (rtrP[X, Y].rtrNextState = chkA1) then
            rtrP[X, Y].rtrNextState := chkA2;
        end;
        chkA2:
        begin
          for i := -r to r do
            for j := -r to r do
              waitQ(X, Y, X + i, Y + j);
          if (rtrP[X, Y].rtrNextState = chkA2) then
            rtrP[X, Y].rtrNextState := sndA;
        end;
        skipA:
        begin
          rtrP[X, Y].rtrNextState := rcvQ;
        end;
      end;
    end;

  for X := 1 to N do
    for Y := 1 to N do
    begin
      rtrP[X, Y].rtrState := rtrP[X, Y].rtrNextState;
      //rtrP[X, Y].rtrNextState := rcvAQ;
    end;

end;

{$R *.lfm}

procedure TForm1.FormCreate(Sender: TObject);
var
  X, Y: integer;
begin
  IW := Image1.Width;
  IH := Image1.Height;
  Zoom := 1;
  Lifetime := 0;
  LT.Text := Format('Life: %d', [Lifetime]);
  N := StrToInt(NN.Text);
  if (N > MAXRTR) then
    N := MAXRTR;
  //Vert.Max := (N div 2);
  //Hor.Max := (N div 2);
  //Vert.Position := (N div 2) - (IW div (Zoom * 2));
  //Hor.Position := (N div 2) - (IH div (Zoom * 2));
  Timer1.Enabled := False;
  SW.Caption := 'Start';
  for X := 1 to N do
    for Y := 1 to N do
    begin
      //P[X, Y] := False;
      rtrP[X, Y].rtrState := rcvQ;
      rtrP[X, Y].rtrNextState := rcvQ;
      rtrP[X, Y].rtrAddr := 0;
      rtrP[X, Y].rtrTime := 0;
    end;
  X := ((N + 1) div 2);//Random(MAXRTR);
  Y := ((N + 1) div 2);//Random(MAXRTR);
  rtrP[X, Y].rtrState := sndQ;
  rtrP[X, Y].rtrAddr := (1 + Random(N)) + (1 + Random(N)) * 256;//sendQ;
  rtrP[X, Y].rtrTime := (1 + Random(N));
  CX := 1;
  CY := 1;
  //X := Vert.Max div 2;
  //Y := Hor.Max div 2;
  //VertScroll(Self, scPosition, X);
  //HorScroll(Self, scPosition, Y);
  Render;
end;

procedure TForm1.Button1Click2(Sender: TObject);
begin
  Lifetime := Lifetime + 1;
  Turn;
  LT.Text := Format('Life: %d', [Lifetime]);
  Render;
end;

procedure TForm1.LTChange(Sender: TObject);
begin

end;

procedure TForm1.LTClick(Sender: TObject);
begin

end;

procedure TForm1.SWClick(Sender: TObject);
begin
  Timer1.Enabled := not Timer1.Enabled;
  if (Timer1.Enabled) then
    SW.Caption := 'Pause'
  else
    SW.Caption := 'Start';
end;

procedure TForm1.Image1Click(Sender: TObject);
var
  X, Y, W, H: integer;
begin
  //W := Width - ClientWidth;
  //H := Height - ClientHeight;
  //X := ((Mouse.CursorPos.X - Form1.Left - Image1.Left) div Zoom) - 1;
  //Y := ((Mouse.CursorPos.Y - Form1.Top - Image1.Top - H + (Zoom + 4)) div Zoom) - 1;
  //P[X + (CX - ((IW div 2) div Zoom)), Y + (CY - ((IH div 2) div Zoom))] :=
  //  not P[X + (CX - ((IW div 2) div Zoom)), Y + (CY - ((IH div 2) div Zoom))];
  //Render;
end;

procedure TForm1.ResClick(Sender: TObject);
begin
  FormCreate(Self);

  FormResize(Self);
end;

procedure TForm1.Image1MouseMove(Sender: TObject; Shift: TShiftState; X, Y: integer);
begin
  //if ssLeft in Shift then
  //  P[X div 4, Y div 4] := True;
  //if ssRight in Shift then
  //  P[X div 4, Y div 4] := False;
  //Render;
end;

procedure TForm1.VertScroll(Sender: TObject; ScrollCode: TScrollCode;
  var ScrollPos: integer);
begin
  CY := 1 + (Vert.Position * (N - 1)) div Vert.Max;
  // ScrollPos + ((IH div 2) div Zoom);
  Render;
end;

procedure TForm1.HorScroll(Sender: TObject; ScrollCode: TScrollCode;
  var ScrollPos: integer);
begin
  CX := 1 + (Hor.Position * (N - 1)) div Hor.Max;
  //CX := Hor.Position; // ScrollPos + ((IW div 2) div Zoom);
  Render;
end;

procedure TForm1.Button1Click(Sender: TObject);
var
  Max, X, Y, BX, BY, YY: integer;
  S: string;
  //T: array [1..1000, 1..1000] of boolean;
  F: TextFile;
begin
  Timer1.Enabled := False;
  Lifetime := Lifetime + 1;
  Turn;
  LT.Text := Format('Life: %d', [Lifetime]);
  Render;
  //if OD.Execute then
  //begin
  //  AssignFile(F, OD.FileName);
  //  ReSet(F);
  //  ReadLn(F);
  //  for X := 1 to 1000 do
  //    for Y := 1 to 1000 do
  //      T[X, Y] := False;
  //  Y := 0;
  //  Max := 0;
  //  while EOF(F) = False do
  //  begin
  //    Y := Y + 1;
  //    ReadLn(F, S);
  //    for X := 1 to Length(S) do
  //      if (S[X] = '*') or (S[X] = 'O') then
  //        T[X, Y] := True;
  //    if Length(S) > Max then
  //      Max := Length(S);
  //  end;
  //  BX := (N div 2) - (Max div 2);
  //  BY := (N div 2) - (Y div 2);
  //  YY := Y;
  //  for X := 1 to Max do
  //    for Y := 1 to YY do
  //      P[BX + X, BY + Y] := T[X, Y];
  //  CloseFile(F);
  //  Render;
  //end;
end;

procedure TForm1.Button2Click(Sender: TObject);
var
  X, Y: integer;
begin
  FormCreate(Self);
  //for X := 0 to N do
  //  for Y := 0 to N do
  //    if Random(5) = 0 then
  //      P[X, Y] := True;
  Timer1.Interval := StrToInt(SPD.Text);
  Lifetime := 0;
  LT.Text := Format('Life: %d', [Lifetime]);
  N := StrToInt(NN.Text);
  for X := 1 to N do
    for Y := 1 to N do
    begin
      rtrP[X, Y].rtrState := rcvQ;
      rtrP[X, Y].rtrNextState := rcvQ;
      rtrP[X, Y].rtrAddr := 0;
      rtrP[X, Y].rtrTime := 0;

    end;

  X := 1 + Random(N);
  Y := 1 + Random(N);
  rtrP[X, Y].rtrState := sndQ;
  rtrP[X, Y].rtrTime := 1 + Random(N);
  rtrP[X, Y].rtrAddr := (1 + Random(N)) + (1 + Random(N)) * 256;//sendQ;

  X := 1 + Random(N);
  Y := 1 + Random(N);
  rtrP[X, Y].rtrState := sndQ;
  rtrP[X, Y].rtrTime := 1 + Random(N);
  rtrP[X, Y].rtrAddr := (1 + Random(N)) + (1 + Random(N)) * 256;//sendQ;
  //rtrP[X, Y].rtrWait := 0;
  // rtrP[(1 + Random(N)), (1 + Random(N))].rtrState := sendQ;
  // rtrP[X, Y].rtrAddr := (1 + Random(N)) + (1 + Random(N)) * 256;//sendQ;
  //NN.Text := IntToStr(X) + ' ' + IntToStr(Y);
  //SPD.Text := IntToStr(rtrP[X, Y].rtrAddr);
  Render;
end;

procedure TForm1.Button3Click(Sender: TObject);
var
  U, V, X, Y: integer;
begin
  Zoom := Zoom + 1;
  //U := N - (IH div Zoom);
  //if U <= 0 then
  //  Vert.Max := 1
  //else
  //  Vert.Max := U;
  //V := N - (IW div Zoom);
  //if V <= 0 then
  //  Hor.Max := 1
  //else
  //  Hor.Max := V;
  //X := Vert.Max div 2;
  //Y := Hor.Max div 2;
  //Vert.Position := X;
  Vert.PageSize := Vert.Max div Zoom div 4;
  if (Vert.PageSize < 2) then
    Vert.PageSize := 2;
  //Hor.Position := X;
  Hor.PageSize := Vert.Max div Zoom div 4;
  if (Hor.PageSize < 2) then
    Hor.PageSize := 2;
  //VertScroll(Self, scPosition, X);
  //HorScroll(Self, scPosition, Y);
  Render;
end;

procedure TForm1.Button4Click(Sender: TObject);
var
  U, V, X, Y: integer;
begin
  Zoom := Zoom - 1;
  if Zoom < 1 then
    Zoom := 1;
  //U := N - (IH div Zoom);
  //if U <= 0 then
  //  Vert.Max := 1
  //else
  //  Vert.Max := U;
  //V := N - (IW div Zoom);
  //if V <= 0 then
  //  Hor.Max := 1
  //else
  //  Hor.Max := V;
  //X := Vert.Max div 2;
  //Y := Hor.Max div 2;
  //Vert.Position := X;
  //Hor.Position := X;

  Vert.PageSize := Vert.Max div Zoom div 4;
  if (Vert.PageSize < 2) then
    Vert.PageSize := 2;
  //Hor.Position := X;
  Hor.PageSize := Vert.Max div Zoom div 4;
  if (Hor.PageSize < 2) then
    Hor.PageSize := 2;
  //VertScroll(Self, scPosition, X);
  //HorScroll(Self, scPosition, Y);
  Render;
end;

procedure TForm1.FormResize(Sender: TObject);
var
  U, V, X, Y: integer;
begin
  Image1.Width := ClientWidth - GroupBox1.Width - Vert.Width - 16;
  Image1.Height := ClientHeight - Hor.Height - 16;
  Hor.Width := Image1.Width - 8;
  Vert.Height := Image1.Height - 8;
  Hor.Top := Image1.Height + 8;
  Vert.Left := Image1.Width + 8;
  GroupBox1.Left := Vert.Left + 8 + Vert.Width;
  Image1.Picture.Bitmap.Width := Image1.Width;
  Image1.Picture.Bitmap.Height := Image1.Height;
  IW := Image1.Width;
  IH := Image1.Height;
  U := N - (IH div Zoom);
  if U <= 0 then
    Vert.Max := 1
  else
    Vert.Max := U;
  V := N - (IW div Zoom);
  if V <= 0 then
    Hor.Max := 1
  else
    Hor.Max := V;
  X := Vert.Max div 2;
  Y := Hor.Max div 2;
  Vert.Position := X;
  Hor.Position := X;
  VertScroll(Self, scPosition, X);
  HorScroll(Self, scPosition, Y);
  Render;
end;

procedure TForm1.Button5Click(Sender: TObject);
begin
  Timer1.Interval := StrToInt(SPD.Text);
end;

end.
